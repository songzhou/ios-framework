//
//  Test.h
//  MyFramework
//
//  Created by Song Zhou on 26/05/2017.
//  Copyright © 2017 Song Zhou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Test : NSObject

- (NSString* )test;

@end
